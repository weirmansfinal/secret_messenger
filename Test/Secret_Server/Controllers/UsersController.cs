﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Shared_Classes;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Secret_Messenger_Server.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        //IDK if readUser() needs to be called every method since the list is static

        //Create a fileclass object to read from file, check and write
        FileClass file = new FileClass();
        //Create list of users that will be returned from reading file
        //Should be static so it persist with every call
        static List<User> users;

        // GET: api/values
        [HttpGet]
        public List<User> Get()
        {
            //Read File and return everything in the users file
            readUsers();
            return users;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public User Get(String id)
        {
            readUsers();

            User userToGet;
            //Get the user that matches the id that was in the uri
            userToGet = users.Where(u => id == u.id).FirstOrDefault();

            return userToGet;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]User value)
        {
            //Add to the list, check if the value has null values
            readUsers();

            //The only way id is null is if the user was never existed
            if(value.id != null)
            {
                users.Add(value);
                updateFile();
            }
            else
            {
                //Tell the user the user value in the json was wrong
            }

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(String id, [FromBody]User value)
        {
            readUsers();

            //find the user in the users list
            var updateUser = users.Where(u => u.id == id).FirstOrDefault();

            //check if that user was found
            if(updateUser != null)
            {
                //update the info since the var is a pointer
                //The only thing the user can edit is password and friendslist
                updateUser.password = value.password;
                updateUser.friends = value.friends;

                updateFile();

            }
            else
            {
                //Tell the user the id didnt work
            }

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(String id)
        {
            readUsers();

            //Get the user using linq
            var userDel = users.Where(u => u.id == id).FirstOrDefault();
            //Check if user was found in list if it wasnt tell the user
            if(userDel != null)
            {
                users.Remove(userDel);

                updateFile();

            }
            else
            {
              //Tell the user
            }
        }

#region helper methods
        //Read everything from the file
        public void readUsers()
        {
            String fileData = "";
            users = new List<User>();

            fileData = file.readFromUserFile();

            users = JsonConvert.DeserializeObject<List<User>>(fileData);
        }


        //Update file
        public void updateFile()
        {
            //Convert users to json the write to file
            var json = JsonConvert.SerializeObject(users);
            file.writeToUserFile(json);
        }


#endregion
    }
}
