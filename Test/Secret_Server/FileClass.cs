﻿using System;
using System.IO;

namespace Secret_Messenger_Server
{
    public class FileClass
    {
        //Strings for userfile name and token file name
        String tokenFile = "tokens.json";
        String userFile = "users.json";

        //Get file path to this project so the files are in the correct location
        String serverPath = Path.GetDirectoryName("Secret_Messenger_Server");

        //Check if all files are created
        private void checkFiles()
        {
            //string for userFile path
            String fullPath = Path.Combine(serverPath, userFile);

            //String for Token File, file to store tokens and user ids
            String tokenPath = Path.Combine(serverPath, tokenFile);

            //Check if the user file exist if it does donothing
            if (!File.Exists(fullPath))
            {
                File.Create(fullPath);
            }

            //Check if the token path if it doesnt create it
            if (!File.Exists(tokenPath))
            {
                File.Create(tokenPath);
            }



        }

        #region token file methods
        //Write to the token file the updated 
        public void writeToTokenFile(String update)
        {
            checkFiles();
            String tokenPath = Path.Combine(serverPath, tokenFile);

            //open a writer to to write to the file
            using (var write = new StreamWriter(tokenPath, false))
            {
                write.Write(update);
                write.Close();
            }

        }

        public String readFromTokenFile()
        {
            checkFiles();
            String jsonData = "";

            String tokenPath = Path.Combine(serverPath, tokenFile);

            using (var streamReader = new StreamReader(tokenPath))
            {
                jsonData = streamReader.ReadToEnd();
                streamReader.Close();
            }

                return jsonData;
        }
        #endregion


        #region user file methods
        //Write to a file and overwrite the file with all new data
        public void writeToUserFile(String update)
        {
            checkFiles();

            String fullPath = Path.Combine(serverPath, userFile);


            //Open a writer to write tot hte file
            using (var writer = new StreamWriter(fullPath, false))
            {
                writer.Write(update);
                writer.Close();
            }

        }


        //Read from the file and return what it had
        public String readFromUserFile()
        {

            checkFiles();
            String fullPath = Path.Combine(serverPath, userFile);

            String jsonData = "";

            using (var streamReader = new StreamReader(fullPath))
            {
                jsonData = streamReader.ReadToEnd();
                streamReader.Close();

            }

            return jsonData;
        }

        #endregion

    }
}
