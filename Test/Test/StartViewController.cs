﻿using System;
using Shared_Classes;
using UIKit;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;

namespace Test
{
    public partial class StartViewController : UIViewController
    {
        HttpClient client;
        public StartViewController() : base("StartViewController", null)
        {
             client = new HttpClient();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        async partial void TestButton_TouchUpInside(UIButton sender)
        {
            List<User> users = new List<User>();
            User user1 = new User("Bobby231", "football");
            Friend f1 = new Friend("Calcolon", Guid.NewGuid().ToString());

            
            var uri = new Uri(string.Format("http://localhost:5000/api/Users"));

            user1.addFriend(f1);


            String json = JsonConvert.SerializeObject(user1);

            Console.WriteLine(json);

            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(uri, content);


            if (response.IsSuccessStatusCode)
            {
                var ac = UIAlertController.Create("POST", "Complete", UIAlertControllerStyle.Alert);
                ac.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                InvokeOnMainThread(
                   () =>
                    {
                        this.PresentViewController(ac, true, null);
                    }
                );
                Console.WriteLine(user1.ToString());
                TestLabel.Text = "It works.";
            }
        }
    }
}

