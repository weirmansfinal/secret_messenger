﻿using System;
using System.Collections.Generic;

namespace Shared_Classes
{



    public class User
    {
        //Need to hold username, password, id, and friends
        //String for username, pass and id
        //We are making the call once the user creates an account they cannot edit it.
        public String username { get; }
        public String password { get; set; }
        public String id { get; }

        //Set for friends because we do not want dupilcates in our friends list
        //The String will just be the friends username and id
        public List<Friend> friends { get; set; }


        //Default constructor should create a new id and a new set
        public User(String un, String pass)
        {
            username = un;
            password = pass;
            id = Guid.NewGuid().ToString();
            friends = new List<Friend>();
        }


        //To String method to print everything out
        public override string ToString()
        {
            String s = "";
            s = "Username " + username + " password: " + password + " id: " + id + " friends: ";
            String f = "";
            //loop to get all values of friends
            foreach(Friend z in friends)
            {
                f += z.ToString() + "\n";
            }

            //Concat both string stogether
            s += f;


            return s;
        }

        //Method to add to friends list
        public void addFriend(Friend f1)
        {
            Friend f = new Friend(f1);
            
            friends.Add(f);
            //Sets do not allow dups, will return false if that friend already
            //Exist in the set or it just fails to add
        }
    }



    //class to hold only id and name of the users contacts
    public class Friend
    {
        String friendName;
        String friendID;

        public Friend(String name, String id)
        {
            friendName = name;
            friendID = id;
        }

        public Friend(Friend f)
        {
            friendName = f.friendName;
            friendID = f.friendID;
        }

        public override string ToString()
        {
            String t = "";
            t = "name: " + friendName + " id: " + friendID;
            return t;
        }
    }

}
